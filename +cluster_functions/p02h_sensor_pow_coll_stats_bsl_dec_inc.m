function p02h_sensor_pow_coll_stats_bsl_dec_inc(data_type, xCond, chooseROI )

% (C) Annekathrin Weise

% clear all;
% 
% xCond  =  'dev_left'; %  'dev_left' 'dev_right'
% 
% chooseROI ='temporal'; % 'temporal', 'parietooccipital'
% data_type = 'meg' 

%% init obob_ownft...
addpath('/mnt/obob/obob_ownft/');
obob_init_ft;

addpath('/mnt/obob/staff/aweise/data/crossfrog/scripts/');
addpath('/mnt/obob/staff/aweise/templates/');

load('aw_neuromag306layout.mat');

doPlot = true;

logtransform = 'yes';

sss = true; 

chooseCycles = 5;

chooseTFmethod = 'wavelet'

chooseMinusAvgTimelock=  'induced'; 


if strcmp(chooseROI, 'temporal')
    freqLimits = [8 14];
elseif strcmp(chooseROI, 'parietooccipital')
    freqLimits = [9 13];
end

timeLimits_dev = [0.4 0.6];
timeLimits_bsl = [-0.6 -0.2];

analysisType =  'pow'
epoch = 'sound';  
avgovertime ='no'; 
avgoverfreq = 'yes';  

statMethod =  'montecarlo' 

if strcmp(statMethod, 'montecarlo')
    nRandomizations = 10000
    if nRandomizations ~= 10000
        warning(['number of randomization is set to: ' num2str(nRandomizations) ]);
    end
end

choose_sensors =  'MEGGRAD'; 

if strcmp(logtransform,'yes')
    sel_sensors_temporal_right = {'MEG1112+1113', 'MEG1122+1123', 'MEG1142+1143'};
    sel_sensors_parietooccipital_left =  {'MEG2012+2013','MEG1632+1633', 'MEG1622+1623'};  
end



if strcmp(chooseROI, 'temporal') 
    choose_sensors_stat =  sel_sensors_temporal_right;
    
elseif strcmp(chooseROI, 'parietooccipital')
    choose_sensors_stat = sel_sensors_parietooccipital_left;
end

subj_ids = functions.get_subject_ids(data_type);
nSubjects = length(subj_ids);


%%  +++++++++++++++++++++++++++++++++++++++++++++
datapath = ['/mnt/obob/staff/aweise/data/crossfrog/data/meg/sensor/' analysisType '/all_sensors/' chooseTFmethod '/' num2str(chooseCycles) 'cycles/logtrans_'  logtransform '/' ];
 
     
if strcmp(statMethod, 'montecarlo')
         outPath = [datapath  chooseMinusAvgTimelock  '/stat_ericmaris/' statMethod '/' num2str(nRandomizations) '/dec_inc_bsl/' ];
elseif strcmp(statMethod, 'analytic')    
        outPath = [datapath   chooseMinusAvgTimelock  '/stat/' statMethod '/dec_inc_bsl/'  ];   
end
if ~exist(outPath, 'dir')
    mkdir(outPath)
end

picPath = [outPath 'pics'];
if ~exist(picPath, 'dir')
    mkdir(picPath)
end

if length(choose_sensors_stat) > 1
     if sss
        singleplotName = ['singleplot_statN' num2str(nSubjects) '_' xCond  '_bsl_' chooseROI '_' num2str(length(choose_sensors_stat)) 'chans_' num2str(freqLimits(1)) '_' num2str(freqLimits(2)) 'Hz_'  num2str(timeLimits_dev(1)) '_' num2str(timeLimits_dev(2)) 's_sss'];
    end
else
    if sss
        singleplotName = ['singleplot_statN' num2str(nSubjects) '_' xCond  '_bsl_' chooseROI '_' chooseChannel '_' num2str(freqLimits(1)) '_' num2str(freqLimits(2)) 'Hz_'  num2str(timeLimits_dev(1)) '_' num2str(timeLimits_dev(2)) 's_sss'];
    end
end



%% collect data of all subj
data = cell(1, nSubjects); % prepares empty matrix

for iSub = 1:nSubjects
    
    % load individual power data (averaged over trials) for each condition into empty matrix
    subj = subj_ids{iSub};
    
    if sss
        inputfile = fullfile(datapath, [subj '_' epoch '_' xCond  '_sss.mat'] );
        
    end
    
    load(inputfile);
    
    % collect data of all subjs
    if strcmp(chooseMinusAvgTimelock, 'minusAvgTimelock')
        data{iSub} =  freq_minus_timelock_logpow;
    elseif strcmp(chooseMinusAvgTimelock, 'induced')
        data{iSub} = freq_logpow;
    elseif strcmp(chooseMinusAvgTimelock, 'timelock')
        data{iSub} =  freq_timelock_logpow;
    end
end


%% select data and create ft structure for baseline activity
for iSubj = 1:nSubjects
    
    % create ft structure for bsl data that contains the mean power of the bsl interval for each sample point present in the data_tmp structure
    cfg = [];
    cfg.latency = timeLimits_bsl ;
    cfg.frequency= freqLimits;
    cfg.avgovertime = 'yes';
    cfg.avgoverfreq ='yes';
    cfg.channel =  choose_sensors_stat;
    cfg.avgoverchan = 'yes';
    
    data_tmp_bsl{iSubj} = ft_selectdata(cfg, data{iSubj} );
    
    % select relevant data to use for cluster based permutation test
    cfg = [];
    cfg.latency = timeLimits_dev ;
    cfg.frequency= freqLimits;
    cfg.avgovertime = 'no';
    cfg.avgoverfreq ='yes';
    cfg.channel =  choose_sensors_stat;
    cfg.avgoverchan = 'yes';
    
    data_dev{iSubj} = ft_selectdata(cfg, data{iSubj} );
    
    data_bsl{iSubj} = data_dev{iSubj}; % initiate ft structure for baseline data that matches the dimensions of the dev ft structure
    length_timevect = length(data_dev{iSubj}.time); % define how many time points in data_dev
    
    % fill the mean bsl power value for each time point and the
    % corresponding channel to get a comparable ft structure as the dev
    % data
    for iTime = 1:length_timevect
        data_bsl{iSubj}.powspctrm(:,:,iTime) = data_tmp_bsl{iSubj}.powspctrm;
    end
        
    data_bsl{iSubj}.time = data_dev{iSubj}.time;
    
end

%% create design matrix based on number of subjects and number of conditions
% N x numobservations: design matrix (for examples/advice, please see the Fieldtrip wiki,
%                                     especially cluster-permutation tutorial and the 'walkthrough' design-matrix section)
% ivar = 2 %  conditions
% uvar = 1 %  subject groups
study_design = [1:nSubjects, 1:nSubjects; ones(1,nSubjects),ones(1,nSubjects)*2];

%% define parameters to analyse the different sensor types (for neuromag:
%  gradiometers and magnetometers)
if strcmp(choose_sensors, 'MEGMAG') == 1
    
    cfg_sensors = [];
    cfg_sensors.types =       'mag';
    cfg_sensors.neighbTemp =  'neuromag306mag_neighb';
    cfg_sensors.channels =    'MEGMAG';
    cfg_sensors.layout	=     'neuromag306mag';
    
elseif strcmp(choose_sensors,'MEGGRAD') == 1
    
    cfg_sensors = [];
    cfg_sensors.types =       'grad';
    cfg_sensors.neighbTemp =  'neuromag306cmb_neighb';
    cfg_sensors.channels =    'MEGGRAD';
    cfg_sensors.layout	=    'neuromag306cmb'; % layout; %
    
    
end

%% create neighbours (necessary if you specify cfg.correctm='cluster' for ft_freq_statistics)

% note on cfg.template for MEG data when caluclating statistics:
% recording MEG data is done via magnetometer and gradiometer. these different sensor types yield totally different things.
% thus, it is not valid to use both within one and the same statistical analysis.
% as long as we stay in the sensor space we need to look at the data from
% different sensors via different statistical analyses;

% note on magnetometer: 1 per position; all in all: 102.
% we can treat them almost similar as EEG sensors (though topographies need to be
% treated differently!)

% note on gradiometer: 2 per position, which point in opposite directions, e.g. one to the front and one to the side.
% to make sense out of the two values one has to combine the information of the two gradiometers
% by using ft_combineplanar without additional options
% ft_combineplanar([], freq) -> this has been done in one of the previous scripts
% after that step the analysis is analog to that of the magnetometers

cfg = [];
cfg.method        = 'template'; % 'distance', 'triangulation' or 'template';
cfg.template = cfg_sensors.neighbTemp; %'neuromag306mag_neighb' or 'neuromag30cmb_neighb'

neighbours = ft_prepare_neighbours(cfg); % neighbourhood structure, see FT_PREPARE_NEIGHBOURS;


%% prepare cfg for stats
if strcmp(statMethod, 'analytic')
    
    cfg = [];
  cfg.parameter   = 'powspctrm';
    cfg.design      = study_design;
    cfg.ivar        = 2; % conditions
    cfg.uvar        = 1; % subjects
    cfg.alpha       = 0.05;  % number, critical value for rejecting the null-hypothesis (default = 0.05)
    cfg.method      = 'analytic';
   cfg.statistic   = 'depsamplesT'; 
    cfg.tail        = 0; %-1;
    cfg.correcttail      = 'prob';     % to test two-tailed: all probabilities (in stat.prob, stat.posclusters.prob and stat.negclusters.prob) are multiplied with a factor of two (mathematically identical to the case when cfg.alpha = .025)
    cfg.clustertail        = 0; %-1;
    
else % montecarlo
    
    cfg = [];
   cfg.parameter   = 'powspctrm'; 
    cfg.method      = 'montecarlo';   
    cfg.correctm    = 'cluster';   
    cfg.neighbours  = neighbours;  
    cfg.design      = study_design;
    cfg.uvar                = 1; % unit variables, i.e. subject groups
    cfg.ivar                = 2; % independent variables, i.e. conditions
    cfg.clusteralpha     = 0.05;
    cfg.clusterstatistic = 'maxsum';
    cfg.alpha            = 0.05;  % number, critical value for rejecting the null-hypothesis (default = 0.05); % check: http://www.fieldtriptoolbox.org/faq/why_should_i_use_the_cfg.correcttail_option_when_using_statistics_montecarlo
    cfg.numrandomization = nRandomizations; %
    cfg.statistic   = 'depsamplesT'; 
    cfg.tail        = 0; 
    cfg.correcttail      = 'prob';     
    cfg.clustertail        = 0; 
end


%% run stat function
freq_stat = ft_freqstatistics(cfg, data_dev{:}, data_bsl{:});

%% plot it
if doPlot
    close all;
    figure(1);
    plot(freq_stat.time, squeeze(freq_stat.stat));
    hold on;
    plot(freq_stat.time, squeeze(freq_stat.mask), 'r');
    
    printfilename = fullfile(picPath, [singleplotName '.png']);
    print(printfilename, '-dpng');
    
end
%%  remove cfg otherwise stat becomes extremely large (> xx GB range)
freq_stat = rmfield(freq_stat, 'cfg');

%% create folder in which data will be stored if not existent
if ~exist(outPath, 'dir')
    mkdir(outPath)
end

%% create stat field in which only significant T values are stored
freq_stat.stat_masked = freq_stat.stat .* freq_stat.mask;

if isempty(freq_stat.stat_masked(:,:,:))
    warning('no significant cluster found')
end


%% save data
if strcmp(analysisType,'pow')
    if sss
        outputfile = fullfile(outPath, ['freq_stat_allN' num2str(nSubjects) '_' xCond '_vs_bsl_roi' chooseROI '_' epoch '_' num2str(freqLimits(1)) '_' num2str(freqLimits(2)) 'Hz_'  num2str(timeLimits_dev(1)) '_' num2str(timeLimits_dev(2)) 's_avgTimeno_avgFreqyes_sss.mat'] );
    end
end
save(outputfile,'freq_stat');

