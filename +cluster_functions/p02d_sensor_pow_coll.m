function p02d_sensor_pow_coll(sss, analysisType, data_type, chooseTFmethod, chooseCycles, choose_sensors,  epoch, xCond, logtransform)


%% init obob_ownft...
addpath('/mnt/obob/obob_ownft/');
obob_init_ft;

addpath('/mnt/obob/staff/aweise/data/crossfrog/scripts/');


%% +++++++++++++++++++++   for testing only   ++++++++++++++++++++++++++
runTest = 'no'; % 'yes' or 'no'

if strcmp(runTest, 'yes')
    chooseCycles = 5;
    logtransform = 'yes';
    sss = true;
    data_type = 'meg';
    subj_ids = functions.get_subject_ids(data_type);
    subj_ids = subj_ids(1)
    chooseTFmethod = 'wavelet';
    
   choose_sensors =  'all_sensors'  
    
    epoch = 'sound';  
    
    analysisType = 'pow'; 
   
    xCond ='dev_right';
end

%% get subject infos
subj_ids = functions.get_subject_ids(data_type);


% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
if strcmp(analysisType , 'pow')
    datapath = ['/mnt/obob/staff/aweise/data/crossfrog/data/meg/sensor/' analysisType '/' choose_sensors '/'  chooseTFmethod '/' num2str(chooseCycles) 'cycles/logtrans_' logtransform '/' ];
elseif strcmp(analysisType , 'erf')
     datapath = ['/mnt/obob/staff/aweise/data/crossfrog/data/meg/sensor/' analysisType '/' choose_sensors '/'  ];
end
nSubjects = length(subj_ids);

data = cell(1, nSubjects); % prepares empty matrix



%% collect data of all subj
for iSub = 1:nSubjects
    
    % load individual power data (averaged over trials) for each condition into empty matrix
    subj = subj_ids{iSub};
    
    if sss
        inputfile = fullfile(datapath, [subj '_' epoch '_' xCond  '_sss.mat'] );
        
    else
        inputfile = fullfile(datapath, [subj '_' epoch '_' xCond  '.mat'] );
    end
    
    if strcmp(analysisType, 'pow')
        load(inputfile);%, 'freq', ' freq_minus_timelock');
   end
    
    % collect data of all subjs
    if strcmp(analysisType, 'pow')
        if strcmp(logtransform, 'yes')
             data{1,iSub} = freq_logpow;
            data_minus_timelockavg{1,iSub} =  freq_minus_timelock_logpow;
            data_timelockavg{1,iSub} =  freq_timelock_logpow;
        elseif strcmp(logtransform, 'no')
            data{1,iSub} = freq;
            data_minus_timelockavg{1,iSub} =  freq_minus_timelock;
            data_timelockavg{1,iSub} =  freq_timelock;
        end
    
    end
    
end


%% do the group average (average across participants) if required
if strcmp(analysisType, 'pow')
    
    cfg = [];
    cfg.keepindividual = 'no'; % or 'no' (default = 'no')
    cfg.foilim         = 'all'; % [fmin fmax] or 'all', to specify a subset of frequencies (default = 'all')
    cfg.toilim         = 'all'; % [tmin tmax] or 'all', to specify a subset of latencies (default = 'all')
    cfg.channel        = 'all'; % Nx1 cell-array with selection of channels (default = 'all'),
    %                           see FT_CHANNELSELECTION for details
    cfg.parameter      = 'powspctrm'; % string or cell-array of strings indicating which
    %                        			parameter(s) to average. default is set to
    %                        			'powspctrm', if it is present in the data.
    
    data_gavg = ft_freqgrandaverage(cfg, data{1,1:end});    
    data_gavg.cfg = []; % delete cfg as otherwise file size is too big!
    
    data_gavg_minus_timelockavg = ft_freqgrandaverage(cfg, data_minus_timelockavg{1,1:end});    
    data_gavg_minus_timelockavg.cfg = []; % delete cfg as otherwise file size is too big!
    
    data_gavg_timelockavg = ft_freqgrandaverage(cfg, data_timelockavg{1,1:end});
    data_gavg_timelockavg.cfg = []; % delete cfg as otherwise file size is too big!
    
end

%% save group average data 
outPath = [datapath 'gavg'];
if ~exist(outPath, 'dir')
    mkdir(outPath)
end

if sss
    outputfile = fullfile(outPath, ['gavgN' num2str(nSubjects) '_' epoch   '_' xCond '_sss.mat'] );
    
else
    outputfile = fullfile(outPath, ['gavgN' num2str(nSubjects) '_' epoch   '_' xCond '.mat'] );
end

if strcmp(analysisType, 'pow')
    
        save(outputfile, 'data_gavg',  'data_gavg_minus_timelockavg','data_gavg_timelockavg');
    
end



end

