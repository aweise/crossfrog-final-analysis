function p02c_sensor_pow(subj_id,  sss,  chooseTFmethod, chooseCycle, choose_sensors,  epoch, choose_sglTrl, logtransform)

% (C) Annekathrin Weise


%% init obob_ownft...
addpath('/mnt/obob/obob_ownft/');
obob_init_ft;

addpath('/mnt/obob/staff/aweise/data/crossfrog/scripts/');


%% +++++++++++++++++++++   for testing only   ++++++++++++++++++++++++++
runTest = 'no'; % 'yes' or 'no'

if strcmp(runTest, 'yes')
    logtransform = 'yes';
    
    choose_sglTrl = 'no'
    
    createWhichFiles = 'createAllFiles';
    
    chooseCycle = 5;
    
    choose_sensors =  'all_sensors'; % 'all_sensors' or  'meggrad'
    
    chooseTFmethod = 'wavelet'; % 'hanningTaper' or 'wavelet'
    
    data_type = 'meg';
    subj_ids = functions.get_subject_ids(data_type);
    subj_ids{1};
    
    epoch = 'sound';
    
    sss = true;
end

% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
if strcmp(choose_sglTrl, 'yes')
    
    outpath_pow = ['/mnt/obob/staff/aweise/data/crossfrog/data/meg/sensor/pow/' choose_sensors '/' chooseTFmethod '/' num2str(chooseCycle) 'cycles/logtrans_' logtransform  '/sglTrl/'   ];
    
elseif strcmp(choose_sglTrl, 'no')
    
    outpath_pow = ['/mnt/obob/staff/aweise/data/crossfrog/data/meg/sensor/pow/' choose_sensors '/' chooseTFmethod '/' num2str(chooseCycle) 'cycles/logtrans_' logtransform '/' ];
end

inpath = '/mnt/obob/staff/aweise/data/crossfrog/data/meg/preproc/clean_ica/';

%% load preprocessed MEG data
if sss
    inputfile = fullfile(inpath, [subj_id '_data_' epoch '_clean_sss']);
end
load(inputfile);

if strcmp(epoch,'sound')
    data = data_sound;
end

%% select smaller data set for testing only
if strcmp(runTest, 'yes')
    cfg = [];
    cfg.trials = 1:100;
    data = ft_selectdata(cfg,data);
end

%% select the data of interest
cfg = [];
if strcmp(choose_sensors,'all_sensors')
    cfg.channel = 'MEG';
else
    cfg.channel = choose_sensors; % irrelevant option on sensor level!
end

data_sel = ft_selectdata(cfg, data);

%% interpolate missing channels / channel repair
if sss ~= true
    
    % 1st NOTE for statistics it is not optimal to interpolate channels; however, as for the neuromag too many channels are rejected
    % there are not so many left across subjects. this would result in big gaps in the topography
    % 2nd NOTE: interpolation needs to be performed before the freq analysis as for some reason this does not work with freq data
    % 3rd NOTE: a workaround for the step interpolation one could replace the missing channels in each subject with NaNs. this would be ok for
    % statistics
    
    % default method used here: replacing them with the average of its neighbours weighted by distance
    % ('nearest'-neighbour approach)
    
    load Cimec_MEG_neigh.mat; % load default neighbours - necessary for interpolating / repairing missing channels
    
    
    cfg = [];
    cfg.neighbours     = [neighbGradY neighbGradX neighbMag];
    cfg.missingchannel = setdiff(data_sel.grad.label, data_sel.label); % find missing channels
    data_sel = ft_channelrepair(cfg, data_sel);
    data_sel.misschan = cfg.missingchannel;
    
end

%% do the spectral analysis for the current stimulus type - but only for epoch type 'sound'
if strcmp(epoch, 'sound')
    
    %% detrend data to remove low frequency drift
    cfg = [];
    cfg.detrend = 'yes';
    
    data_detrend= ft_preprocessing(cfg, data_sel);
    
    
    %% get relevant conditions
    conds =  {'dev_left', 'dev_right'};
    
    for iCondition = 1:length(conds)
        
        xCondition = conds{iCondition};
        
        cond_codes = functions.get_cond_codes(xCondition,epoch);
        
        
        %% get relevant trial indices
        ind_rel_trials = find(ismember(data_detrend.trialinfo(:,4),cond_codes));
        
        % Increase sensitivity to non-phase locked signals
        
        % One of the purposes of TFR analysis is to reveal signals that are non-phase locked to stimuli, in contrast to the analysis of
        % evoked fields/potentials. But the phase-locked signals also show up in the TFRs, as they are present in the signal and therefore
        % decomposed by the TF analysis. To enhance the sensitivity to the non-phase locked activity, we can subtract the phase locked activity
        % and redo the analysis.
        % The phase-locked activity is the evoked field/potential. Assuming the average evoked signal are a good representation of the
        % phase-locked signal (i.e., sufficient signal-to-noise ratio), we can subtract it from each trial, and redo the TFR analysis.
        
        % Get time-locked
        cfg = [];
        cfg.trials = ind_rel_trials;
        data_timelock = ft_timelockanalysis(cfg, data_detrend);
        
        data_minus_timelock = data_detrend;
        for i = 1:length(data_detrend.trial)
            data_minus_timelock.trial{i} = data_detrend.trial{i} - data_timelock.avg;
        end
        
        if strcmp(chooseTFmethod, 'hanningTaper')
            % do TF analysis
            cfg = [];
            
            cfg.trials = ind_rel_trials; % relevant trials of the current stimuluy type; default: all
            
            cfg.method = 'mtmconvol'; % fourier transform;
            %                         % in combination with option 'hanning'
            %                         % this function implements a singletaper
            %                         % time frequency transformation!
            
            cfg.foi = 5:1:30; % frequencies of interest. starts at 1Hz and goes up to 30Hz in 1Hz steps
            
            cfg.taper = 'hanning'; %  The option 'hanning' ensures that only
            % 				          one taper will be applied thereby not introducing
            %						  an artificial frequency smoothing
            
            % ++++++++++++++++++ define the size of the window ++++++++++++
            % define the size of the window and thus also the frequency resolution
            
            cfg.t_ftimwin = chooseCycle ./cfg.foi; % The size of the window: delta t = 5/f in sec..
            %                             In this case, every window is 5 cycles long.
            %                             cfg.t_ftimwin  = vector 1 x numfoi, length of time window (in seconds)
            
            % +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            
            
            cfg.toi = -1.5 : 0.050 : 1.5; % Time bins from the beginning to the end of the respective epoch
            
            cfg.output =  'pow'; % return the power-spectra
            
            cfg.channel = 'all'; % default: 'all'; % channels have been specified above!
            
            if strcmp(choose_sglTrl, 'yes')
                cfg.keeptrials = 'yes';
            elseif strcmp(choose_sglTrl, 'no')
                cfg.keeptrials = 'no'; % default: no; ''fourier'' requires cfg.keeptrials = ''yes'' and cfg.keeptapers = ''yes'''
                %   cfg.keeptrials = 'yes' or 'no', return individual trials or average (default = 'no')
                % keep trials also relevant when doing e.g. correlation with behavior
            end
            
            cfg.keeptapers = 'no'; % default: no; ''fourier'' requires cfg.keeptrials = ''yes'' and cfg.keeptapers = ''yes'''
            %   cfg.keeptapers = 'yes' or 'no', return individual tapers or average (default = 'no')
            
            % +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            
            if strcmp(logtransform,'yes')
                cfg.keeptrials = 'yes';
            end
            
        elseif strcmp(chooseTFmethod, 'wavelet')
            
            % TFR with Morlet wavelets
            cfg = [];
            cfg.method = 'wavelet';
            cfg.trials = ind_rel_trials; % relevant trials of the current stimuluy type; default: all
            cfg.foi         = 5:1:30;       % Frequencies we want to estimate from 1 Hz to 30 Hz in steps of 1HZ
            cfg.toi         =  -1.5 : 0.05 : 1.5; % Time bins from the beginning to the end of the respective epoch
            cfg.width    = chooseCycle;
            cfg.output =  'pow'; % return the power-spectra
            
            if strcmp(choose_sglTrl, 'yes')
                cfg.keeptrials = 'yes';
            elseif strcmp(choose_sglTrl, 'no')
                cfg.keeptrials = 'no'; % default: no; ''fourier'' requires cfg.keeptrials = ''yes'' and cfg.keeptapers = ''yes'''
                %   cfg.keeptrials = 'yes' or 'no', return individual trials or average (default = 'no')
                % keep trials also relevant when doing e.g. correlation with behavior
            end
            
            if strcmp(logtransform,'yes')
                cfg.keeptrials = 'yes';
            end
            
        end
        
        freq = ft_freqanalysis(cfg, data_detrend);
        freq.cfg = []; % remove cfg field in freq
        
        freq_minus_timelock = ft_freqanalysis(cfg, data_minus_timelock);
        freq_minus_timelock.cfg = [];
        
        cfg.trials  = 'all';
        freq_timelock = ft_freqanalysis(cfg, data_timelock);
        freq_timelock.cfg = [];
        
        %% combine data of gradiometer
        % computes the planar gradient magnitude over both directions
        % combining the two gradients at each sensor to a single positive-valued number.
        % This can be done for averaged planar gradient single-trial TFR (i.e. powerspectra).
        
        % note: recording MEG data is done via magnetometer and gradiometer. these different sensor types yield totally different things.
        % note on gradiometer: 2 per position, which point in opposite directions, e.g. one to the front and one to the side.
        % to make sense out of the two values one has to combine the information of the two gradiometers
        % by using ft_combineplanar without additional options  ft_combineplanar([], freq)
        freq  = ft_combineplanar([], freq );
        freq_minus_timelock  = ft_combineplanar([], freq_minus_timelock);
        freq_timelock  = ft_combineplanar([], freq_timelock);
        
        
        %% logtransoform data if option is set to 'yes'
        if strcmp(logtransform,'yes')
            %             Log-transform the single-trial power
            %             Spectral power is not normally distributed. Although this is in theory not a problem
            %             for the non-parametric statistical test, its sensitivity is usually increased by
            %             log-transforming the values in the power spectrum.
            
            
            cfg           = [];
            cfg.parameter = 'powspctrm';
            cfg.operation = 'log10';
            
            freq_logpow    = ft_math(cfg, freq);
            freq_timelock_logpow = ft_math(cfg, freq_timelock);
            freq_minus_timelock_logpow = ft_math(cfg, freq_minus_timelock);
            
            if strcmp(choose_sglTrl, 'no')
                
                % get rid of single trials to save storage space
                cfg = [];
                cfg.trials = 'all';
                cfg.avgoverrpt = 'yes';
                
                freq_logpow = ft_selectdata(cfg,freq_logpow);
                freq_minus_timelock_logpow = ft_selectdata(cfg,freq_minus_timelock_logpow);
                freq_timelock_logpow = ft_selectdata(cfg,freq_timelock_logpow);
            end
        end
        
        
        
        %% create folder in which data will be stored if not existent
        if ~exist(outpath_pow, 'dir')
            mkdir(outpath_pow)
        end
        
        %% save individual power data for each condition
        if sss
            outputfile = fullfile(outpath_pow, [subj_id '_' epoch '_' xCondition  '_sss.mat'] );
        else
            outputfile = fullfile(outpath_pow, [subj_id '_' epoch '_' xCondition  '.mat'] );
        end
        
        if strcmp(logtransform,'yes')
            save(outputfile, 'freq_logpow', 'freq_minus_timelock_logpow','freq_timelock_logpow');%'-v7.3');
            clear xCondition cond_codes ind_rel_trials freq_logpow freq_minus_timelock_logpow data_timelock  data_minus_timelock 'freq_timelock_logpow'
        else
            save(outputfile, 'freq', 'freq_minus_timelock','freq_timelock');%'-v7.3');
            clear xCondition cond_codes ind_rel_trials freq freq_minus_timelock data_timelock  data_minus_timelock 'freq_timelock'
        end
        
    end % cond loop
end % if epoch

