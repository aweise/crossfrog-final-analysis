clear all global;
close all;

% init obob_ownft...
addpath('/mnt/obob/obob_ownft/');

cfg=[];
cfg.package.svs=true;
cfg.package.gm2 = true;
obob_init_ft(cfg);

addpath('/mnt/obob/staff/aweise/data/crossfrog/scripts/');

chooseBrainOption =  'parcel';
load('aw_parcelLayout_3mm');

%% +++++++++++++++++++++++++++++++++++++++++++++

%    ------------------- FIX PARAMETER! --------------------------
logtransform = 'yes';
regfac = '5%';
chooseTFmethod = 'wavelet'
freqBand = 'alphaFreq';
chooseHemisphere = 'bothHemis'; 
pValue = 0.05;
leadfieldNormOption =  'no'  ; 
ica = true;
sss = true;
ssp = false;
choose_sensors =  'meggrad'; 
contrastType = 'devL_devR'; 
analysisType =  'pow'
epoch = 'sound';  
avgovertime = 'yes'; 
avgoverfreq = 'yes';  
data_type =    'meg'; 

%    ------------------- SPECIFY PARAMETER! --------------------------

doPlot = 1;

% choose roi!
chooseROI =   'parietooccipital'; % 'temporal' , 'parietooccipital'

% choose!
chooseMinusAvgTimelock =  'minusAvgTimelock'; % 'induced', 'minusAvgTimelock';

freqLimits = [8 14];
timeLimits = [0.2 0.6];

statMethod = 'analytic'; 

subj_ids = functions.get_subject_ids(data_type);
nSubjects = length(subj_ids);


%%
datapath = ['/mnt/obob/staff/aweise/data/crossfrog/data/meg/source/' chooseBrainOption '/' analysisType '/norm_' leadfieldNormOption '/' choose_sensors '/' chooseTFmethod '/logtrans_' logtransform '/'  ];

outPath = [datapath  'stat/' statMethod '/' chooseMinusAvgTimelock '/'  ];
if strcmp(chooseHemisphere , 'leftHemi') || strcmp(chooseHemisphere , 'rightHemi')
    outPath = [datapath  'stat/' chooseHemisphere '/'  statMethod '/' chooseMinusAvgTimelock '/'   ];
end

if ~exist(outPath, 'dir')
    mkdir(outPath)
end

% create folder in which data will be stored if not existent
picsPath = [outPath '/pics/'];
if ~exist(picsPath, 'dir')
    mkdir(picsPath)
end

if strcmp(contrastType, 'devL_devR')
    contrastConds =  {'dev_left', 'dev_right' };
end

%% load data
for iCond = 1:length(contrastConds)
    
    xCond = contrastConds{iCond};
    
    data = cell(1, nSubjects); % prepares empty matrix
    data_minus_timelockavg = cell(1, nSubjects); % prepares empty matrix
    for iSub = 1:nSubjects
        
        % load individual power data (averaged over trials) for each condition into empty matrix
        subj = subj_ids{iSub};
        inputfile = fullfile(datapath, [subj '_' epoch '_' xCond  '_sss_ica.mat'] );
        load(inputfile);        
        
        % collect data of all subjs
        if strcmp(chooseMinusAvgTimelock, 'minusAvgTimelock')
            data_minus_timelockavg{iSub} =  freq_minus_timelock_logpow;
        elseif strcmp(chooseMinusAvgTimelock, 'induced')
            data{iSub} = freq_logpow;
        end        
        
    end
    
    if strcmp(chooseMinusAvgTimelock, 'minusAvgTimelock')
        contrast_conds.(xCond) = data_minus_timelockavg;
    elseif strcmp(chooseMinusAvgTimelock, 'induced')
        contrast_conds.(xCond) = data;
    end
end

devL_norm = cell(1, nSubjects); % prepares empty matrix
devR_norm = cell(1, nSubjects); % prepares empty matrix

for iSubj = 1:nSubjects
        % note: do not normalize data for logtransformed alpha pow values        
        devL_norm{iSubj} = contrast_conds.dev_left{iSubj};
        devR_norm{iSubj} = contrast_conds.dev_right{iSubj};        
end

%% create design matrix based on number of subjects and number of conditions
% N x numobservations: design matrix (for examples/advice, please see the Fieldtrip wiki,
%                                     especially cluster-permutation tutorial and the 'walkthrough' design-matrix section)
% ivar = 2 %  conditions
% uvar = 1 %  subject groups

study_design = [1:nSubjects, 1:nSubjects; ones(1,nSubjects),ones(1,nSubjects)*2];

%% get mni grid of 3 mm resolution (note: if you change the grid [resolution]: change it also for the analysis steps before)
load parcellations_3mm.mat; % layout for parcellation approach


%% prepare cfg for source stats
if strcmp(statMethod, 'analytic')
    
    cfg = [];
    if strcmp(chooseHemisphere , 'rightHemi')
        cfg.channel = layout.right.label;
    elseif strcmp(chooseHemisphere , 'leftHemi')
        cfg.channel = layout.left.label;
    else
        cfg.channel = 'all';
    end
    
    cfg.latency = timeLimits;
    cfg.frequency = freqLimits;
    cfg.avgovertime = avgovertime;
    cfg.avgoverfreq = avgoverfreq;
    cfg.parameter   = 'powspctrm';
    cfg.design      = study_design;
    cfg.ivar        = 2; % conditions
    cfg.uvar        = 1; % subjects
    cfg.alpha       = pValue;  
    cfg.method      = 'analytic';
    cfg.statistic   = 'depsamplesT';    
    if strcmp(chooseROI,'parietooccipital')
        cfg.tail        = 1;
    elseif strcmp(chooseROI,'temporal')
        cfg.tail        = -1;
    else
        cfg.tail        = 0;
    end    

end

%% run stat function
data_stat = ft_freqstatistics(cfg, devL_norm{:}, devR_norm{:});
    
%%  remove cfg otherwise stat becomes extremely large (> xx GB range)
data_stat = rmfield(data_stat, 'cfg');

%% create stat field in which only significant T values are stored
data_stat.stat_masked = data_stat.stat .* data_stat.mask;

% name sign channels / parcels
if strcmp(chooseROI,'parietooccipital')
    data_stat.ind_chan_sig = find(data_stat.stat >= data_stat.critval);
    data_stat.max_data = max(data_stat.stat)
elseif  strcmp(chooseROI,'temporal')
    data_stat.ind_chan_sig = find(data_stat.stat <= data_stat.critval);
    data_stat.min_data = min(data_stat.stat)
else
    data_stat.ind_chan_sig = find(data_stat.mask > 0);
end
data_stat.label_sign_chan = data_stat.label(data_stat.ind_chan_sig)

%critval: -1.6991 / 1.6991

%% save data
outputfile = fullfile(outPath, ['data_stat_allN' num2str(nSubjects) '_sss_ica_' chooseROI '_'  contrastType '_' choose_sensors '_' epoch '_' num2str(freqLimits(1)) '_' num2str(freqLimits(2)) 'Hz_'  num2str(timeLimits(1)) '_' num2str(timeLimits(2)) 's_avgTime' avgovertime '_avgFreq' avgoverfreq '.mat'] );       
save(outputfile,'data_stat');

%% plot cluster

if doPlot == 1
    
    %% cbrewer has a range of nice colormaps.
    
    % perceptually great colormap
    colorcetmap= functions.colorcet('BWRA');
    colorcetmap_hot = colorcetmap(129:end,:);
    colorcetmap_cold = colorcetmap(1:128,:);
    
    
    % interpolate parcels on a mri
    load parcellations_3mm.mat;
    load('standard_mri_segmented');
    
    %     load atlas
%             load atlas_parcel333.mat
%             atlas.tissuelabel = atlas.brick0label;
    atlas = ft_read_atlas('/mnt/obob/obob_ownft/external/fieldtrip/template/atlas/aal/ROI_MNI_V4.nii'); 
    
    %
    cfg = [];
    if strcmp(chooseBrainOption, 'parcel')
        cfg.sourcegrid = parcellation.parcel_grid;
    else
        load mni_grid0.0075greyMatter.mat;
        cfg.sourcegrid = mni_grid_greyMatter;
    end
    cfg.parameter = 'stat'; % 'stat_masked';
    cfg.mri = mri_seg.bss; % A brain, scull, scalp segmentation
    cfg.latency = timeLimits; % make sure to use the range in which sig cluster was found (check via multiplot)
    cfg.frequency = freqLimits;% make sure to use the range in which sig cluster was found (check via multiplot)
    
    data_parc_interpol = obob_svs_virtualsens2source(cfg, data_stat);
    data_parc_interpol.coordsys = 'mni';
    src = data_parc_interpol;
    
    if strcmp(chooseROI,'parietooccipital')
        src.mask = src.stat >= data_stat.critval;
    elseif  strcmp(chooseROI,'temporal')
        src.mask = src.stat <= data_stat.critval;
    else
        src.mask = src.stat <= data_stat.critval(1);
%         src.mask =  src.stat >= data_stat.critval(2) ;
        
        
    end
   
    src.finalmask = src.mask & src.brain_mask;
    
    %% make ugly plot    
    cfg = [];
    cfg.funparameter = 'stat';
    cfg.maskparameter = 'finalmask';
    cfg.atlas = atlas;
    if strcmp(chooseROI,'parietooccipital')
        cfg.location  = 'max';
    elseif strcmp(chooseROI,'temporal')
        cfg.location  = 'min';
    end

    ft_sourceplot(cfg, src);
    
    %% make nice source plot
    close all;
    
    xFile = [ 'sourceplot_devL_devR_N' num2str(nSubjects) '_' statMethod '_' chooseROI '_' num2str(freqLimits(1)) '_' num2str(freqLimits(2)) 'Hz_' num2str(timeLimits(1)) '_' num2str(timeLimits(2)) 's_sss' num2str(sss) '_ica' num2str(ica)];
    
    dpi=300;
    cfg = [];
    if strcmp(chooseROI, 'parietooccipital')
        if strcmp(data_type, 'meg')            
             if strcmp(chooseMinusAvgTimelock,'induced')
                 cfg.funcolorlim = [data_stat.critval 3.54]; 
             else
                 cfg.funcolorlim = [data_stat.critval 3.58]; 
             end
       end
        cfg.funcolormap = colorcetmap_hot(6:end, :,:);        
    elseif strcmp(chooseROI, 'temporal')
         if strcmp(data_type, 'meg')            
            if strcmp(chooseMinusAvgTimelock,'induced')
                cfg.funcolorlim = [-2.78 data_stat.critval]; 
            else
                cfg.funcolorlim = [-3.01 data_stat.critval]; 
            end
        end
         cfg.funcolormap = colorcetmap_cold(1:end-6,:,:);
    else
        cfg.funcolormap = colorcetmap;
    end
    
    cfg.funparameter = 'stat';
    cfg.maskparameter = 'mask';
    cfg.method =  'surface';  
    cfg.camlight = 'no';
    cfg.projmethod = 'nearest'; 
    
    if strcmp(chooseROI, 'parietooccipital')
        % (1) make figures for left hemisphere
      
        cfg.surffile = 'surface_white_left.mat'; 
        ft_sourceplot(cfg, src);
        
        functions.plot_caret_style(fullfile(picsPath, [xFile '_lefthem_ft_temporal']), dpi, [], 'left');         
        png_name = [picsPath xFile '_lefthem_ft_temporal_left.png'];
        eval(['print -dpng -r' num2str(300) ' ' png_name]);
        
        
        
        %% 
        view(90,0);
        camlight(camlight('left'), 'right');
        png_name = [picsPath xFile '_lefthem_ft_temporal_right.png'];
        eval(['print -dpng -r' num2str(300) ' ' png_name]);
        
        %%
        close all;
        cfg.surffile = 'surface_white_left.mat'; 
        ft_sourceplot(cfg, src);
        material([0.3,0.9,0.1,10,1]); 
        view(-74,10);
        camlight(camlight('right'), 'left');
        png_name = [picsPath xFile '_lefthem_ft_occipital_left.png'];
        eval(['print -dpng -r' num2str(300) ' ' png_name]);
        
        %%
        close all;
        cfg.surffile = 'surface_white_left.mat'; 
        ft_sourceplot(cfg, src);
        material([0.3,0.9,0.1,10,1]); 
        view(74,10);
        camlight(camlight('left'), 'right');
        png_name = [picsPath xFile '_lefthem_ft_occipital_right.png'];
        eval(['print -dpng -r' num2str(300) ' ' png_name]);
        
        
        
    elseif strcmp(chooseROI, 'temporal')
        % (2) make figures for right hemisphere
       
        cfg.surffile = 'surface_white_right.mat'; 
        ft_sourceplot(cfg, src);
                
        functions.plot_caret_style(fullfile(picsPath, [xFile '_righthem_ft_temporal']), dpi, [], 'right');         
        png_name = [picsPath xFile '_righthem_ft_temporal_right.png'];
        eval(['print -dpng -r' num2str(300) ' ' png_name]);       
        
        %% 
        view(-90,0);
        camlight(camlight('right'), 'left');
        png_name = [picsPath xFile '_righthem_ft_temporal_left.png'];
        eval(['print -dpng -r' num2str(300) ' ' png_name]);
        
        %%
        close all
        cfg.surffile = 'surface_white_right.mat'; 
        ft_sourceplot(cfg, src);
        material([0.3,0.9,0.1,10,1]); 
        view(74,10);
        camlight(camlight('left'), 'right');
        png_name = [picsPath xFile '_righthem_ft_occipital_right.png'];
        eval(['print -dpng -r' num2str(300) ' ' png_name]);
        
        %%
        close all
        cfg.surffile = 'surface_white_right.mat'; 
        ft_sourceplot(cfg, src);
        material([0.3,0.9,0.1,10,1]); 
        view(-74,10);
        camlight(camlight('right'), 'left');
        png_name = [picsPath xFile '_lefthem_ft_occipital_left.png'];
        eval(['print -dpng -r' num2str(300) ' ' png_name]);
        
        
        
    end    
    
    
else
    
    
        cfg.surffile = 'surface_white_left.mat'; 
        ft_sourceplot(cfg, src);
        
        functions.plot_caret_style(fullfile(picsPath, [xFile '_lefthem_ft_temporal']), dpi, [], 'left');         
        png_name = [picsPath xFile '_lefthem_ft_temporal_left.png'];
        eval(['print -dpng -r' num2str(300) ' ' png_name]);
        
        
        
        %% 
        view(90,0);
        camlight(camlight('left'), 'right');
        png_name = [picsPath xFile '_lefthem_ft_temporal_right.png'];
        eval(['print -dpng -r' num2str(300) ' ' png_name]);
        
       
        
        
end
