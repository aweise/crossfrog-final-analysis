
clear all global;
close all;

%% init obob_ownft...
addpath('/mnt/obob/obob_ownft/');
obob_init_ft;

%% define vars
data_type = 'behave';
subj_ids = functions.get_subject_ids(data_type); 
trialinfo_path = '/mnt/obob/staff/aweise/data/crossfrog/data/meg/trialinfo/sta_binaural/';

trial_onset_code = 201;
sound_onset_code= [2,3,4,5,20,120,220,30,130,230,40,140,240,50,150,250];
novels_right = [3, 5];
sounds_deviant = [2,3,4,5];
targets_right = [4,5,40,140,240,50,150,250];
targets_left = [2,3,20,120,220,30,130,230];
sta_bef_dev = [120,130,140,150];
sta_without_sta_after_dev = [20, 30, 40, 50, 120,130,140,150];

target_onset_code = [203,204];
rt_onset_datapixx_code = {'STI013', 'STI014'};
response_code = [111, 221, 112, 222, 113, 223]; % correct, incorrect, no response for left and right target, respectively
response_code_correct = [111, 221];
response_code_incorrect = [112, 222];
response_code_noresponse  = [113, 223];

n_blocks = 7; % number experimental blocks
sss = false;
nr_subject_id = 1; 


%% subj loop
for iSubj = nr_subject_id:length(subj_ids)

    subj_id = subj_ids{iSubj};
    subj_id_ext = ['subj_' subj_id]; 
    
    for iFile = 1:n_blocks

        summary_trials = [];
        trial_matrix = [];
            
        fif_name = functions.get_fif_names( subj_id, iFile, sss);   
        
        % read events
        hdr = ft_read_header(fif_name);

        events = ft_read_event(fif_name, 'header', hdr);

        events = ft_filter_event(events, 'type', {'Trigger', 'STI013', 'STI014'});   %  'STI013' -> codes for right target;  'STI014'  --> codes for left target

       % trigger for right sound preceding target has same value as datapixx response trigger (=5)
        % to avoid confusion we first re-code this vipixxtrigger
        for iEvent = 1:length(events)

            curr_event_type = events(iEvent).type;
            
            if strcmp(curr_event_type,'STI013') 
                events(iEvent).value =  5013;
            elseif strcmp(curr_event_type,'STI014') 
              events(iEvent).value =  5014;
            end
            
        end
        
        event_trialStartTrigger = ft_filter_event(events, 'type', 'Trigger', 'value', trial_onset_code);

        all_blocktrials = length(event_trialStartTrigger);

        idx_events = 0;
        idx_trials = 0;

        relevant_events = length(events);

        while (idx_events+1) <= length(events)

            idx_events = idx_events + 1;

            % check for trial onset code
            if events(idx_events).value ~= trial_onset_code                
                continue;
            else
                idx_trials = idx_trials + 1;
                timing.trialonset = events(idx_events).sample;
            end

            % check that response was not given following the start of the
            % next trial
            trigger_type = events(idx_events + 1).type;
            if strcmp(trigger_type, rt_onset_datapixx_code(1)) || strcmp(trigger_type, rt_onset_datapixx_code(2))
                % summary_trials(end+1, :) = [idx_trials, trial_onset_code, timing.trialonset, sound_trigger, timing.sound, target_trigger, timing.target, response_trigger, response_correct, rt,  response_trigger_2nd, sound_deviant, sound_right, target_right, snd_target_locations_congruent];
                summary_trials(end+1, :) = [idx_trials, trial_onset_code, timing.trialonset, 0, 0, 0,0, 0,0 , 0,  0, 0, 0, 0, 0 ];

                % trial_matrix(end+1, :) = [idx_trials, timing.sound, sound_trigger, sound_deviant, sound_right, target_right, snd_target_locations_congruent, response_trigger, response_correct, rt];
                trial_matrix(end+1, :) = [idx_trials, 0, 666, 0, 0, 0, 0, 0, 0, 0, 0];

                continue
            end

            % check for sound code
            sound_trigger = events(idx_events + 1).value;
            if ~ismember(sound_trigger, sound_onset_code)
                error('trigger does not code for sound onset. check why!')
            else
                timing.sound = events(idx_events + 1).sample;
            end

            % check for target code
            target_trigger = events(idx_events + 2).value;   % should either be 203 (left vis. target) or 204 (right vis. target)
            if ~ismember(target_trigger, target_onset_code)
                if strcmp(events(idx_events + 2).type, rt_onset_datapixx_code(1)) || strcmp(events(idx_events + 2).type, rt_onset_datapixx_code(2))
                    % summary_trials(end+1, :) = [idx_trials, trial_onset_code, timing.trialonset, sound_trigger, timing.sound, target_trigger, timing.target, response_trigger, response_correct, rt,  response_trigger_2nd, sound_deviant, sound_right, target_right, snd_target_locations_congruent];
                    summary_trials(end+1, :) = [idx_trials, trial_onset_code, timing.trialonset, sound_trigger, timing.sound, target_trigger, timing.target, 0, 0, 0,  0, 0, 0, 0, 0 ];

                    % trial_matrix(end+1, :) = [idx_trials, timing.sound, sound_trigger, sound_deviant, sound_right, target_right, snd_target_locations_congruent, response_trigger, response_correct, rt];
                    trial_matrix(end+1, :) = [idx_trials, timing.sound, sound_trigger, 0, 0, 0, 0, 0, 0, 0, 0];
                    continue
                else
                    error('trigger does not code for target onset. check why!')
                end
            else
                timing.target = events(idx_events + 2).sample;
            end

            % check for rt code
            rt_trigger = events(idx_events + 3).type;
            response_codex = events(idx_events + 3).value;
            if ~strcmp(rt_trigger, rt_onset_datapixx_code) % if no response: continue with event loop
                rt = 0;
                if strcmp(rt_trigger, trial_onset_code)
                    %                     summary_trials = [summary_trials; idx_trials, trial_onset_code, timing.trialonset, sound_trigger, timing.sound, target_trigger, timing.target, response_trigger, response_correct, rt,  response_trigger_2nd ];
                    summary_trials(end+1, :) = [idx_trials, trial_onset_code, timing.trialonset, sound_trigger, timing.sound, target_trigger, timing.target, 0, 0, rt,  0, 0, 0, 0, 0 ];

                    % trial_matrix(end+1, :) = [idx_trials, timing.sound, sound_trigger, sound_deviant, sound_right, target_right, snd_target_locations_congruent, response_trigger, response_correct, rt];
                    trial_matrix(end+1, :) = [idx_trials, timing.sound, sound_trigger, 0, 0, 0, 0, 0, 0, rt, timing.target]; % rt = 0

                    continue
                elseif ismember(response_codex, response_code)
                    % summary_trials(end+1, :) = [idx_trials, trial_onset_code, timing.trialonset, sound_trigger, timing.sound, target_trigger, timing.target, response_trigger, response_correct, rt,  response_trigger_2nd, sound_deviant, sound_right, target_right, snd_target_locations_congruent];
                    summary_trials(end+1, :) = [idx_trials, trial_onset_code, timing.trialonset, sound_trigger, timing.sound, target_trigger, timing.target, 0, 0, rt,  0, 0, 0, 0, 0 ];

                    % trial_matrix(end+1, :) = [idx_trials, timing.sound, sound_trigger, sound_deviant, sound_right, target_right, snd_target_locations_congruent, response_trigger, response_correct, rt];
                    trial_matrix(end+1, :) = [idx_trials, timing.sound, sound_trigger, 0, 0, 0, 0, 0, 0, rt, timing.target]; % rt = 0
                    continue
                else
                    error('trigger does not code for rt onset. check why!')
                end
            else % otherwise save rt
                timing.response = events(idx_events + 3).sample;
                rt = timing.response - timing.target;
            end

            % only use trials in which responses were given in response window
            if rt <= 100
                rt = 0;
            elseif rt > 800
                rt = 0;
            end

            % check whether response is correct or false
            response_trigger = events(idx_events + 4).value;  % response code: correct or false
            if ~ismember(response_trigger, response_code)
                trigger_type = events(idx_events + 4).type;
                if strcmp(trigger_type, rt_onset_datapixx_code(1)) || strcmp(trigger_type, rt_onset_datapixx_code(2)) % if participant responded twice
                    % summary_trials(end+1, :) = [idx_trials, trial_onset_code, timing.trialonset, sound_trigger, timing.sound, target_trigger, timing.target, response_trigger, response_correct, rt,  response_trigger_2nd, sound_deviant, sound_right, target_right, snd_target_locations_congruent];
                    summary_trials(end+1,:) = [idx_trials, trial_onset_code, timing.trialonset, sound_trigger, timing.sound, target_trigger, timing.target, response_trigger, 0, rt,  0, 0, 0, 0, 0 ];

                    % trial_matrix(end+1, :) = [idx_trials, timing.sound, sound_trigger, sound_deviant, sound_right, target_right, snd_target_locations_congruent, response_trigger, response_correct, rt];
                    trial_matrix(end+1,:) = [idx_trials, timing.sound, sound_trigger, 0, 0, 0, 0, response_trigger, 0, 0, timing.target];
                    continue;
                else
                    error('trigger does not code for response code. check why!')
                end
            else
                if ismember(response_trigger, response_code_correct)
                    response_correct = 1;
                elseif ismember(response_trigger, response_code_incorrect) || ismember(response_trigger, response_code_noresponse)
                    response_correct = 0;
                    rt = 0;
                end

                if idx_trials ~= all_blocktrials % if it is NOT the last trial in the file / block
                    response_trigger_2nd = events(idx_events + 5).value;
                    if ismember(response_trigger_2nd, response_code)
                        warning('participant responded two times. trial invalid.')
                        response_correct = 0;
                    end
                end
            end

            % when trial structure is fine, check remaining stuff: sounds_right, sounds_deviant, targets_right, snd_target_locations_congruent
            if ismember(sound_trigger, novels_right)
                sound_right = true; % code 1
            else
                sound_right = false; % code 0
            end

            if ismember(sound_trigger, sounds_deviant)
                sound_deviant = true; % code 1
            else
                sound_deviant = false; % code 0
            end

            if ismember(sound_trigger, targets_right) % note: sound trigger also codes for location of target!
                target_right = true; % code 1
            else
                target_right = false; % code 0
            end

            if sound_right == target_right
                snd_target_locations_congruent = true;
            else
                snd_target_locations_congruent = false;
            end

            % save summary of trials for control purposes
            summary_trials(end+1, :) = [idx_trials, trial_onset_code, timing.trialonset, sound_trigger, timing.sound, target_trigger, timing.target, response_trigger, response_correct, rt,  response_trigger_2nd, sound_deviant, sound_right, target_right, snd_target_locations_congruent];

            trial_matrix(end+1, :) = [idx_trials, timing.sound, sound_trigger, sound_deviant, sound_right, target_right, snd_target_locations_congruent, response_trigger, response_correct, rt, timing.target];

        end % while loop

        summary_trials_all{iFile} = summary_trials;
        trialinfo{iFile} = trial_matrix;

    end % file / block loop


    %% save output
    output_filename = [subj_id '_trialinfo'];
    if ~exist(trialinfo_path,'dir')
        mkdir(trialinfo_path)
    end
    save(fullfile(trialinfo_path, output_filename), 'summary_trials_all', 'trialinfo');

end
