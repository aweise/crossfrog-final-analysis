
clear all global;
close all;

% init obob_ownft...
addpath('/mnt/obob/obob_ownft/');

cfg=[];
obob_init_ft;

addpath('/mnt/obob/staff/aweise/data/crossfrog/scripts/');
addpath('/mnt/obob/staff/aweise/templates/');

%% % general graphics, this will apply to any figure you open
% (groot is the default figure object).
set(groot, ...
    'DefaultFigureColor', 'w', ...
    'DefaultAxesLineWidth', 0.8, ...
    'DefaultAxesXColor', 'k', ...
    'DefaultAxesYColor', 'k', ...
    'DefaultAxesFontUnits', 'points', ...
    'DefaultAxesFontSize', 14, ...
    'DefaultAxesFontName', 'Calibri', ...
    'DefaultLineLineWidth', 1, ...
    'DefaultTextFontUnits', 'Points', ...
    'DefaultTextFontSize', 14, ...
    'DefaultTextFontName', 'Calibri', ...
    'DefaultAxesBox', 'off', ...
    'DefaultAxesTickLength', [0.02 0.025]);

% set the tickdirs to go out - need this specific order
set(groot, 'DefaultAxesTickDir', 'out');
set(groot, 'DefaultAxesTickDirMode', 'manual');

% perceptually great colormap
colorcetmap= functions.colorcet('BWRA');
colorcetmap_hot = colorcetmap(129:end,:);
colorcetmap_cold = colorcetmap(1:128,:);

% fix variables
logtransform = 'yes';
sss = true;
windowType = 'wavelet'; 
chooseCycles = 5; 
data_type = 'meg'; 
subj_nr = '30';

% choose one of the following two options!
induced = false; 
minusAvgTimelock = true; 

% choose roi!
choose_roi = 'temporal'; % 'parietooccipital', 'temporal'

inpath = [ '/mnt/obob/staff/aweise/data/crossfrog/data/meg/sensor/pow/all_sensors/' windowType  '/' num2str(chooseCycles) 'cycles/logtrans_' logtransform '/gavg/'];

if induced    
         picpath = [ '/mnt/obob/staff/aweise/data/crossfrog/data/meg/sensor/pow/all_sensors/' windowType  '/' num2str(chooseCycles) 'cycles/induced/logtrans_' logtransform '/ms/pics/'];
    
elseif minusAvgTimelock      
          picpath = [ '/mnt/obob/staff/aweise/data/crossfrog/data/meg/sensor/pow/all_sensors/' windowType  '/' num2str(chooseCycles) 'cycles/logtrans_' logtransform '/minusAvgTimelock/ms/pics/'];     
          
end

if ~exist(picpath,'dir')
    mkdir(picpath);
end

if induced
   
        % load gavg data
        load([inpath 'gavgN' subj_nr '_sound_dev_left_sss.mat']);
        gavg.devL = data_gavg; clear data_gavg;
        
        load([inpath 'gavgN' subj_nr '_sound_dev_right_sss.mat']);
        gavg.devR = data_gavg; clear data_gavg;
    
elseif minusAvgTimelock
    
        % load gavg data
        load([inpath 'gavgN' subj_nr '_sound_dev_left_sss.mat']);
        gavg.devL = data_gavg_minus_timelockavg; clear data_gavg_minus_timelockavg;
        
        load([inpath 'gavgN' subj_nr '_sound_dev_right_sss.mat']);
        gavg.devR = data_gavg_minus_timelockavg; clear data_gavg_minus_timelockavg;
   
end      
    
% calc diff of gavg
gavg.devL_devR = gavg.devL;
gavg.devL_devR.powspctrm = gavg.devL.powspctrm - gavg.devR.powspctrm ;
    

%% get the best channels from the cluster statistic
if strcmp(choose_roi , 'temporal')
    if strcmp(data_type, 'meg')
       good_chan = {'MEG1112+1113', 'MEG1122+1123', 'MEG1142+1143'}; 
    end
    
    
elseif strcmp(choose_roi , 'parietooccipital')
    if strcmp(data_type, 'meg')
        good_chan =  {'MEG2012+2013','MEG1622+1623', 'MEG1632+1633'};   
    end
    
end

% ********************************************************************************
% set variabes for the topoplot
startFreqTopo = 8;
endFreqTopo = 14;
startTimeTopo = 0.2;
endTimeTopo = 0.6;
% set variables for the time frequency plot
startTime = 0;
endTime = 0.7;
stepsTime = 0.1;
startFreq = 5;
endFreq = 30;
stepsFreq = 4;

singleplotName = ['singleplot_gavgN' subj_nr '_devL_devR_' choose_roi '_' num2str(length(good_chan)) 'chans_' num2str(startFreq) '_' num2str(endFreq) 'Hz_'  num2str(startTime) '_' num2str(endTime) 's_sss'];
    
topoplotName_gavg =['topoplot_gavgN' subj_nr '_devL_devR_' choose_roi '_' num2str(startFreqTopo) '_' num2str(endFreqTopo) 'Hz_'  num2str(startTimeTopo) '_' num2str(endTimeTopo) 's_sss'];

%% plot topoplot  - gavg und 3 channels with max(?) stats!
% select relevant time range
close all;
cfg = [];
cfg.latency = [startTimeTopo endTimeTopo];
cfg.frequency = [startFreqTopo endFreqTopo];

if strcmp(choose_roi , 'temporal')
    gavg_topo = ft_selectdata(cfg, gavg.devL_devR );
elseif strcmp(choose_roi , 'parietooccipital')
    gavg_topo = ft_selectdata(cfg, gavg.devL_devR );
end

% make plot
cfg = [];
cfg.colorbar = 'yes';
load('neuromag306cmb_helmet_modLabels');
cfg.layout	=    layout; %'neuromag306cmb';
cfg.parameter = 'powspctrm'; %'stat';
cfg.comment   = 'no';
cfg.style =  'straight_imsat'; %'straight_imsat';   'both_imsat';
cfg.gridscale = 300;
cfg.renderer = 'painters';
cfg.colormap =  colorcetmap; 
cfg.zlim = [-0.03 0.03];
cfg.colorbar =  'SouthOutside'; 
cfg.highlightsize      = 11;
cfg.highlightchannel   = {'MEG2012+2013','MEG1622+1623', 'MEG1632+1633','MEG1112+1113', 'MEG1122+1123', 'MEG1142+1143'};%good_chan;
cfg.highlightsymbol    = '*';
cfg.highlight  = 'on'; 

cfg.parameter = 'powspctrm';
ft_topoplotTFR(cfg, gavg_topo); % plot gavg

% printfilename = fullfile(picpath, [topoplotName_gavg '.png']);
% print(printfilename, '-dpng');

printfilename = fullfile(picpath, [topoplotName_gavg '.svg']);
print(printfilename, '-dsvg');


%% select relevant time range for single TF plot
cfg = [];
cfg.latency = [startTime endTime];
cfg.frequency = [startFreq endFreq];
cfg.channel = good_chan; 
cfg.avgoverchan = 'yes';
gavg_sel.devL_devR = ft_selectdata(cfg, gavg.devL_devR );


%% make nice paper plot
close all;
data = gavg_sel.devL_devR;
dep_var = squeeze(data.powspctrm);

figure();
imagesc(data.time, data.freq, dep_var);
xlim([startTime endTime]); % to hide the NaN values at the edges of the TFR
axis xy; % to have the y-axis extend upwards

% Label the principal axes:
xlabel('Time (s)');
ylabel('Frequency (Hz)');

% Set colour limits symmetric around zero:
if induced
    clim = 0.045; %max(abs(dep_var(:)));
else
        clim = 0.05; %max(abs(dep_var(:)));
end

caxis([-clim clim]);

colormap(colorcetmap);
h = colorbar();
ylabel(h, 'power');

% The finer time and frequency axes:
tim_interp = linspace(startTime, endTime, 512);
freq_interp = linspace(startFreq, endFreq, 512);

% We need to make a full time/frequency grid of both the original and
% interpolated coordinates. Matlab's meshgrid() does this for us:
[tim_grid_orig, freq_grid_orig] = meshgrid(data.time, data.freq);
[tim_grid_interp, freq_grid_interp] = meshgrid(tim_interp, freq_interp);

% And interpolate:
dep_var_interp = interp2(tim_grid_orig, freq_grid_orig, dep_var,...
    tim_grid_interp, freq_grid_interp, 'spline');

imagesc(tim_interp, freq_interp, dep_var_interp);
xlim([startTime endTime]); %xtick(startTime:0.1:endTime);
axis xy;
xlabel('Time (s)');
ylabel('Frequency (Hz)');
if induced
    clim = 0.045; %max(abs(dep_var(:)));
else
    clim = 0.05; %max(abs(dep_var(:)));
end
caxis([-clim clim]);
colormap(colorcetmap);
h = colorbar();
ylabel(h, 'log-transformed power');

% Let's also add a line at t = 0s for clarity:
hold on;
plot(zeros(size(freq_interp)), freq_interp, 'k:');

% printfilename = fullfile(picpath, [singleplotName '.png']);
% print(printfilename, '-dpng');
printfilename = fullfile(picpath, [ singleplotName '.svg' ]);
print(printfilename, '-dsvg');




