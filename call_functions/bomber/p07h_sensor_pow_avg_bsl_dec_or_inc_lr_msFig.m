
clear all global;

% init obob_ownft...
addpath('/mnt/obob/obob_ownft/');

cfg=[];
obob_init_ft;

addpath('/mnt/obob/staff/aweise/data/crossfrog/scripts/');
addpath('/mnt/obob/staff/aweise/templates/');

% general graphics, this will apply to any figure you open
% (groot is the default figure object).
set(groot, ...
    'DefaultFigureColor', 'w', ...
    'DefaultAxesLineWidth', 0.8, ...
    'DefaultAxesXColor', 'k', ...
    'DefaultAxesYColor', 'k', ...
    'DefaultAxesFontUnits', 'points', ...
    'DefaultAxesFontSize', 14, ...
    'DefaultAxesFontName', 'Calibri', ...
    'DefaultLineLineWidth', 2, ...
    'DefaultTextFontUnits', 'Points', ...
    'DefaultTextFontSize', 14, ...
    'DefaultTextFontName', 'Calibri', ...
    'DefaultAxesBox', 'off', ...
    'DefaultAxesTickLength', [0.02 0.025]);

% set the tickdirs to go out - need this specific order
set(groot, 'DefaultAxesTickDir', 'out');
set(groot, 'DefaultAxesTickDirMode', 'manual');

%% +++++++++++++++++++++++++++++++++++++++++++++

%    ------------------- FIX PARAMETER! --------------------------
logtransform = 'yes';
sss = true; 
chooseCycles = 5; 
chooseTFmethod = 'wavelet'; 
chooseMinusAvgTimelock= 'induced'  ; 
cond_names =  {'dev_left', 'dev_right'}; %
bslWin = [-0.6 -0.2];
analysisWin_temporal = [0.2 0.6];
analysisWin_parietooccipital = [0.2 0.6];
freqLimits_temporal = [8 14]; 
freqLimits_parietooccipital = [9 13]; 
timeLimits = [-0.7 0.7]; % for plotting
avgovertime = 'no';
avgoverfreq = 'yes';
avgoverchan = 'yes';
analysisType =  'pow'
epoch = 'sound';  

sel_sensors_temporal_right = {'MEG1112+1113', 'MEG1122+1123', 'MEG1142+1143'};
sel_sensors_parietooccipital_left = {'MEG2012+2013','MEG1622+1623', 'MEG1632+1633'};

data_type = 'meg'; 
subj_ids = functions.get_subject_ids(data_type);
nSubjects = length(subj_ids);

%%  +++++++++++++++++++++++++++++++++++++++++++++
datapath = ['/mnt/obob/staff/aweise/data/crossfrog/data/meg/sensor/' analysisType '/all_sensors/' chooseTFmethod '/' num2str(chooseCycles) 'cycles/logtrans_' logtransform '/' ];

%% create folder in which data will be stored if not existent
outPath = [datapath '/' chooseMinusAvgTimelock '/left_vs_right'];

if ~exist(outPath, 'dir')
    mkdir(outPath)
end

%% collect data of all subj
for iCond = 1:length(cond_names)
    
    xCond = cond_names{iCond};
    
    data = cell(1, nSubjects); % prepares empty matrix
    
    for iSub = 1:nSubjects
        
        % load individual power data (averaged over trials) for each condition into empty matrix
        subj = subj_ids{iSub};
        inputfile = fullfile(datapath, [subj '_' epoch '_' xCond  '_sss.mat'] );
        
        load(inputfile);
        
        % collect data of all subjs
        if strcmp(chooseMinusAvgTimelock, 'minusAvgTimelock')
            if strcmp(logtransform, 'yes')
                data{1,iSub} =  freq_minus_timelock_logpow;
            end
        elseif strcmp(chooseMinusAvgTimelock, 'induced')
            if strcmp(logtransform, 'yes')
                data{1,iSub} = freq_logpow;           
            end
        end
        
        % select data for plotting
        cfg = [];
        cfg.latency = timeLimits; 
        cfg.avgovertime = avgovertime;
        cfg.avgoverfreq =avgoverfreq;
        cfg.avgoverchan = avgoverchan;
       
        
        % process data                 
        cfg.channel = sel_sensors_parietooccipital_left;
        cfg.frequency= freqLimits_parietooccipital;
        
        data_plot_tmp.(xCond).parietooccipital_left{iSub} = ft_selectdata(cfg,data{iSub} );
        data_plot_coll.(xCond).parietooccipital_left(iSub,:) = data_plot_tmp.(xCond).parietooccipital_left{iSub}.powspctrm;
        
        % ***************************************************
        
        cfg.channel = sel_sensors_temporal_right;
        cfg.frequency= freqLimits_temporal; 
        
        data_plot_tmp.(xCond).temporal_right{iSub} = ft_selectdata(cfg,data{iSub} );
        data_plot_coll.(xCond).temporal_right(iSub,:) = data_plot_tmp.(xCond).temporal_right{iSub} .powspctrm;
      
        
    end
    
    %% calculate grand average for plotting   
     % process bsl data
     data_plot_gavg.(xCond).parietooccipital_left(1,:) = mean(data_plot_coll.(xCond).parietooccipital_left);
     
     data_plot_gavg.(xCond).temporal_right(1,:) = mean(data_plot_coll.(xCond).temporal_right);
    
end
 

%% plot data
timevect = [-0.700000000000000,-0.648000000000000,-0.600000000000000,-0.548000000000000,-0.500000000000000,-0.448000000000000,-0.400000000000000,-0.348000000000000,-0.300000000000000,-0.248000000000000,-0.200000000000000,-0.148000000000000,-0.100000000000000,-0.0480000000000000,0,0.0480000000000000,0.100000000000000,0.148000000000000,0.200000000000000,0.248000000000000,0.300000000000000,0.348000000000000,0.400000000000000,0.448000000000000,0.500000000000000,0.548000000000000,0.600000000000000,0.648000000000000,0.700000000000000];

figure (2);
subplot(2,1,1); 
hold on
plot(timevect,data_plot_gavg.dev_left.temporal_right(1,:), 'b', 'LineWidth',2)
plot(timevect,data_plot_gavg.dev_right.temporal_right(1,:), 'r', 'LineWidth',2);
xlim([-0.7 0.7]);

subplot(2,1,2); 
hold on
plot(timevect,data_plot_gavg.dev_left.parietooccipital_left(1,:), 'r', 'LineWidth',2)
plot(timevect,data_plot_gavg.dev_right.parietooccipital_left(1,:), 'b', 'LineWidth',2);
xlim([-0.7 0.7]);

% printfilename = fullfile(outPath, ['left_vs_right_inc_vs_decN' num2str(nSubjects) '.png']);
% print(printfilename, '-dpng');
printfilename = fullfile(outPath, [ 'left_vs_right_inc_vs_decN' num2str(nSubjects)  '.svg' ]);
print(printfilename, '-dsvg');


