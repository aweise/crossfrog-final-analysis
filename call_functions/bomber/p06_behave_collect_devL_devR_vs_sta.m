clear all global;
close all;

%% paths
addpath('/mnt/obob/staff/aweise/data/crossfrog/scripts/call_functions/bomber');
addpath('/mnt/obob/staff/aweise/data/crossfrog/scripts/');

trialinfo_sorted_path = '/mnt/obob/staff/aweise/data/crossfrog/data/meg/trialinfo/sta_binaural/sorted/';
trialinfo_path = '/mnt/obob/staff/aweise/data/crossfrog/data/meg/trialinfo/sta_binaural/';
res_path = '/mnt/obob/staff/aweise/data/crossfrog/data/meg/trialinfo/sta_binaural/res/';

if ~exist(res_path, 'dir')
    mkdir(res_path)
end

outPath = [res_path 'pics/sta_vs_dev/'];
if ~exist(outPath,'dir')
    mkdir(outPath)
end

% define subjects
data_type = 'meg';
subj_ids = functions.get_subject_ids(data_type);
nr_subject_id = 1;

nSubjects = length(subj_ids);

response_correct = 1;
sta_bef_dev = [120,130,140,150]; % sta preceding congruent devs
devL = [2,4];
devR = [3,5];
devLR = [2,3,4,5];
% standards = [20,30,40,50, 120,130,140,150, 220,230,240,250]; % all standards incl. sta bef deviant and sta after deviant
% max_trials_oneLocation = 56;
% max_trials_bothLocations = 112;
% vis_target_right coded in column 4 of summary;
min_rt = 100;
max_rt = 800;

n_bl_trials = 160;

vars = {'devL', 'sta_b_devL', 'devR', 'sta_b_devR'};

doPlot = 0;


%% re-structure data for behavioral analysis
% subj loop
for iSubj = nr_subject_id:length(subj_ids)
    
    subj_id = subj_ids{iSubj};
    subj_id_ext = ['subj_' subj_id];
    
    % load data
    input_filename = [subj_id '_trialinfo'];
    load([trialinfo_path input_filename]);
    
    % concatenate data of all blocks
    nBlocks = length(trialinfo);
    data_all = [];
    for iBlock = 1:nBlocks
        curr_data = trialinfo{iBlock};
        data_all = [data_all; curr_data];
    end
    
    ind_devR = find(ismember(data_all(:,3), devR));
    data.devR = data_all(ind_devR,:);
    
    ind_sta_b_devR_tmp = ind_devR-1;
    data_sta_b_devR_tmp = data_all(ind_sta_b_devR_tmp,:);
    % in case there is for some technical reason no sta-bef-dev (coded as
    % 120,130,140 or 150) delete from matrix!
    ind_sta_b_devR = find(ismember(data_sta_b_devR_tmp(:,3), sta_bef_dev));
    data.sta_b_devR = data_sta_b_devR_tmp(ind_sta_b_devR ,:);
    
    ind_devL = find(ismember(data_all(:,3), devL));
    data.devL = data_all(ind_devL,:);
    
    ind_sta_b_devL_tmp = ind_devL-1;
    data_sta_b_devL_tmp = data_all(ind_sta_b_devL_tmp,:);
    % in case there is for some technical reason no sta-bef-dev (coded as
    % 120,130,140 or 150) delete from matrix!
    ind_sta_b_devL = find(ismember(data_sta_b_devL_tmp(:,3), sta_bef_dev));
    data.sta_b_devL = data_sta_b_devL_tmp(ind_sta_b_devL ,:);
    
    
    ind_devLR = find(ismember(data_all(:,3), devLR));
    data.devLR = data_all(ind_devLR,:);
    
    ind_sta_b_devLR_tmp = ind_devLR-1;
    data_sta_b_devLR_tmp = data_all(ind_sta_b_devLR_tmp,:);
    % in case there is for some technical reason no sta-bef-dev (coded as
    % 120,130,140 or 150) delete from matrix!
    ind_sta_b_devLR = find(ismember(data_sta_b_devLR_tmp(:,3), sta_bef_dev));
    data.sta_b_devLR = data_sta_b_devLR_tmp(ind_sta_b_devLR ,:);
    
    
    
    for iVar = 1:length(vars)
        % column 3: sound_code,
        % column 9: response_correct, % 1 = correct; 0 = incorrect; also is set to 0 if RT = 0 (because RT outside response time window, etc)
        % column 10: reaction_time
        
        curr_var = vars{iVar}
        
        curr_var = vars{iVar}        
        % find suspicious trials and delete from analysis matrix
        ind_suspicious_trials = find(data.(curr_var)(:,10) == 0);
        for iTr = 1:length(ind_suspicious_trials)
            curr_Tr = ind_suspicious_trials(iTr );
            % RT must not be zero when there was a correct response
            % (in that case RT had been coded in a previous analysis step to zero)
            % and those trials need to be removed from analysis here
            if data.(curr_var)(curr_Tr,9) == response_correct % if HIT for suspicious trial (i.e. with RT = zero): delete trial from analysis matrix!
                data.(curr_var)(curr_Tr,:)
                disp([ num2str(curr_Tr) 'will be deleted']);
                data.(curr_var)(curr_Tr,:)=[];
                ind_suspicious_trials = ind_suspicious_trials - 1;
            end
        end
        
        % if response code is missing [111,112,221,222] 
        % delete trial from analysis matrix!
        ind_suspicious_trials = find(data.(curr_var)(:,8) == 0);
        for iTr = 1:length(ind_suspicious_trials)
            curr_Tr = ind_suspicious_trials(iTr );
            if data.(curr_var)(curr_Tr,8) == 0 
                data.(curr_var)(curr_Tr,:)
                disp([ num2str(curr_Tr) 'will be deleted']);
                data.(curr_var)(curr_Tr,:)=[];
                ind_suspicious_trials = ind_suspicious_trials - 1;
            end
        end
               
        
        nr_trials = length(data.(curr_var)(:,9)); % all trial incl. incorrect responses
        
        indices_response_correct = find(data.(curr_var)(:,9) == response_correct);
        correct_responses = data.(curr_var)(indices_response_correct,:); % take RTs only from correct responses
        indices_correct_rt_win = find(correct_responses(:,10) > min_rt & correct_responses(:,10) < max_rt);
        
        reaction_times.(curr_var).(subj_id_ext) = correct_responses(indices_correct_rt_win,10);
        
        median_reaction_time.(curr_var).(subj_id_ext) = median(reaction_times.(curr_var).(subj_id_ext) );
        median_reaction_time_coll.(curr_var)(iSubj) = median(reaction_times.(curr_var).(subj_id_ext));
       
        nresponse_correct_rt_win = length(indices_correct_rt_win);
        detection_rate.(curr_var).(subj_id_ext)  = nresponse_correct_rt_win/nr_trials;
        detection_rate_coll.(curr_var)(iSubj)  = nresponse_correct_rt_win/nr_trials;
        
        nIncorr_responses = nr_trials - nresponse_correct_rt_win;
        
    end % subj loop
    
end

output_filename = ['res_devL_devR_devLR_vs_sta_N' num2str(nSubjects)];
save(fullfile(res_path, output_filename),   'detection_rate_coll',  'median_reaction_time_coll' );


RTs_dev_sta = [];
RTs_dev_sta = [median_reaction_time_coll.devL' median_reaction_time_coll.sta_b_devL' median_reaction_time_coll.devR' median_reaction_time_coll.sta_b_devR'];
xlswrite([res_path 'medianRTs_dev_sta.xls'],RTs_dev_sta)


detect_rate_dev_sta = [];
detect_rate_dev_sta = [detection_rate_coll.devL' detection_rate_coll.sta_b_devL' detection_rate_coll.devR' detection_rate_coll.sta_b_devR'];
xlswrite([res_path 'detection_rate_dev_sta.xls'],detect_rate_dev_sta)







