clear all global;

% init obob_ownft...
addpath('/mnt/obob/obob_ownft/');

cfg=[];
obob_init_ft;

addpath('/mnt/obob/staff/aweise/data/crossfrog/scripts/');
addpath('/mnt/obob/staff/aweise/templates/');

load('aw_neuromag306layout.mat');

%% +++++++++++++++++++++++++++++++++++++++++++++

%    ------------------- SPECIFY PARAMETER! --------------------------
logtransform = 'yes';
sss = true; 
chooseCycles = 5; 
chooseTFmethod = 'wavelet'; 
freqLimits = [8 14]; 
timeLimits = [0.2 0.6];
analysisType =  'pow'
epoch = 'sound';  
statMethod =  'analytics'

choose_sensors =  'MEGGRAD'; 
choose_sensors_LH_roi = {'MEG2012+2013','MEG1622+1623', 'MEG1632+1633'};
choose_sensors_RH_roi = {'MEG2022+2023', 'MEG2442+2443', 'MEG2412+2413'};
data_type = 'meg' ; 
subj_ids = functions.get_subject_ids(data_type);
nSubjects = length(subj_ids);

chooseMinusAvgTimelock=   'induced' ; % 'induced' or 'minusAvgTimelock'

contrastType = 'devL_devR';


%%  +++++++++++++++++++++++++++++++++++++++++++++

datapath = ['/mnt/obob/staff/aweise/data/crossfrog/data/meg/sensor/' analysisType '/all_sensors/' chooseTFmethod '/' num2str(chooseCycles) 'cycles/logtrans_' logtransform  '/' ];

outPath = [datapath   chooseMinusAvgTimelock  '/stat/' statMethod '/'  ];


if strcmp(contrastType, 'devL_devR') 
    contrastConds =  {'dev_left', 'dev_right' };   
end


%% collect data of all subj
for iCond = 1:length(contrastConds)
    
    xCond = contrastConds{iCond};
    
    data = cell(1, nSubjects); % prepares empty matrix
    
    for iSub = 1:nSubjects
        
        % load individual power data (averaged over trials) for each condition into empty matrix
        subj = subj_ids{iSub};
        
        if sss
            inputfile = fullfile(datapath, [subj '_' epoch '_' xCond  '_sss.mat'] );
        end
        
        load(inputfile);
        
        % collect data of all subjs
        if strcmp(logtransform, 'yes')
            if strcmp(chooseMinusAvgTimelock, 'minusAvgTimelock')
                data_minus_timelockavg{1,iSub} =  freq_minus_timelock_logpow;
            elseif strcmp(chooseMinusAvgTimelock, 'induced')
                data{1,iSub} = freq_logpow;
            elseif strcmp(chooseMinusAvgTimelock, 'timelock')
                data_timelockavg{1,iSub} =  freq_timelock_logpow;
            end
            
        end
        
        if strcmp(chooseMinusAvgTimelock, 'minusAvgTimelock')
            contrast_conds.(xCond) = data_minus_timelockavg;
        elseif strcmp(chooseMinusAvgTimelock, 'induced')
            contrast_conds.(xCond) = data;
        elseif strcmp(chooseMinusAvgTimelock, 'timelock')
            contrast_conds.(xCond) = data_timelockavg;
        end
    end
end
contrast_conds

ami_lh_values = [];
ami_rh_values = [];
% normalize power
for iSubj = 1:nSubjects
    
    if strcmp(contrastType, 'devL_devR')
        
        if strcmp(logtransform, 'yes')
            % note: no need to normalize the logtransformed 
            devL_norm{iSubj} = contrast_conds.dev_left{iSubj};            
            cfg = [];
            cfg.frequency = freqLimits;
            cfg.avgoverfreq = 'yes';
            cfg.latency = timeLimits;
            cfg.avgovertime = 'yes';
            cfg.channel = choose_sensors;
            devL_norm{iSubj} = ft_selectdata(cfg,devL_norm{iSubj} );
            
                       
            devR_norm{iSubj} = contrast_conds.dev_right{iSubj};            
            cfg = [];
            cfg.frequency = freqLimits;
            cfg.avgoverfreq = 'yes';
            cfg.latency = timeLimits;
            cfg.avgovertime = 'yes';
            cfg.channel = choose_sensors;
            devR_norm{iSubj} = ft_selectdata(cfg,devR_norm{iSubj} );
            
            AMI = devL_norm;
            AMI{iSubj}.powspctrm = (devL_norm{iSubj}.powspctrm - devR_norm{iSubj}.powspctrm);% ./ abs(devL_norm{iSubj}.powspctrm + devR_norm{iSubj}.powspctrm);
        
            % select left hemispheric channels
            cfg = [];
            cfg.channel = choose_sensors_LH_roi;
            cfg.avgoverchan = 'yes';
            ami_lh{iSubj} = ft_selectdata(cfg, AMI{iSubj});
            
            
            % select right hemispheric channels
            cfg = [];
            cfg.channel = choose_sensors_RH_roi;
            cfg.avgoverchan = 'yes';
            ami_rh{iSubj} = ft_selectdata(cfg, AMI{iSubj});
            
            ami_lh_values(iSubj) = ami_lh{iSubj}.powspctrm;
            ami_rh_values(iSubj) = ami_rh{iSubj}.powspctrm;
            
        end
    end
end


% kstest - test for normal distribution
normal_lh = kstest(ami_lh_values) % not normal!
normal_rh = kstest(ami_rh_values) % not normal!


%[H,pval,CI,STATS] = ttest(ami_lh_values, ami_rh_values,'tail','left') 
[pval,H, STATS] = signrank(ami_lh_values, ami_rh_values, 'tail', 'right') % one-tailed t test - median is greater than zero

mean_lh_li  = mean(ami_lh_values')
mean_rh_li  = mean(ami_rh_values')

std_lh_li  = std(ami_lh_values')
std_rh_li  = std(ami_rh_values')

% median_lh_ami  = median(ami_lh_values')
% median_rh_ami  = median(ami_rh_values')
% 
% iqr_lh_ami  = iqr(ami_lh_values')
% iqr_rh_ami  = iqr(ami_rh_values')
% 
% sem_lh_ami  = std(ami_lh_values) / sqrt(length(ami_lh_values))
% sem_rh_ami  = std(ami_rh_values) / sqrt(length(ami_rh_values))
