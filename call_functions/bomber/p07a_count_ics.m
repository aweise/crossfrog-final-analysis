%% clear
clear all global
close all


%% init obob_ownft
addpath('/mnt/obob/obob_ownft/');

cfg = [];

obob_init_ft(cfg);

%% set variables...
sss =true; 
data_type = 'meg'; 
subj_ids = functions.get_subject_ids(data_type);

%% ------------------------------------------

for iSubj = 1:length(subj_ids)
    
    subj_id = subj_ids{iSubj}
    disp(subj_id)
    
    for iBlock =  1:7
        
        disp(['block0' num2str(iBlock)]);
        
        %  set paths and load data....
        
        inpath = ['/mnt/obob/staff/aweise/data/crossfrog/data/meg/preproc/ica/' subj_id '/'];
        outpath = ['/mnt/obob/staff/aweise/data/crossfrog/data/meg/preproc/ica/'];
        
        if sss
            load(fullfile(inpath, ['file_0' num2str(iBlock) '_sss_rej_comp']), 'rejected_components', 'subj_id', 'block');
        else
            load(fullfile(inpath, ['file_0' num2str(iBlock) '_rej_comp']), 'rejected_components', 'subj_id', 'block');
        end
        
        subj_name = ['subj_' subj_id];
        
        n_rej_comps = length(rejected_components);
        n_rej_comps_allBlocks.(subj_name)(iBlock,:) = n_rej_comps;
        
        
    end
    avg_rej_comp_iSubj_iBlock.(subj_name) = mean(n_rej_comps_allBlocks.(subj_name))
    avg_rej_comp_iSubj_avgBlock(iSubj) = mean(n_rej_comps_allBlocks.(subj_name))
end
avg_rej_comp_iSubj_avgBlock = avg_rej_comp_iSubj_avgBlock';

avg_rej_comp_avgSubj_avgBlock = mean(avg_rej_comp_iSubj_avgBlock); % ans = 4.6122 when NO sss applied

% save data...
if ~exist(outpath,'dir')
    mkdir(outpath);
end

if sss
    save(fullfile(outpath, [data_type  '_sss_avg_rej_comp']), 'avg_rej_comp_iSubj_iBlock', 'avg_rej_comp_iSubj_avgBlock', 'avg_rej_comp_avgSubj_avgBlock');
end
disp('data saved');





