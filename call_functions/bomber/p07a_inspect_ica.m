%% clear
clear all global
close all

% update: 3/4/2019: adopted to identifiy components for maxfiltered data

%% init obob_ownft
addpath('/mnt/obob/obob_ownft/');

cfg = [];

obob_init_ft(cfg);

%% set variables...
sss = true; 
data_type = 'meg';
subj_ids = functions.get_subject_ids(data_type);

%% DEFINE DATA TO USE
% ______________________________________________________________________
disp('******************************************************************');
disp('*');
warning('your input required');
disp('*');
disp('******************************************************************');
prompt = 'Define subj_id and block to use! [y]: ';
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end

%% ------------------------------------------
subj_id = subj_ids{30}; 
block = 1;
disp(['block0' num2str(block)]);

%  set paths and load data....

inpath = ['/mnt/obob/staff/aweise/data/crossfrog/data/meg/preproc/ica/' subj_id '/'];
outpath = ['/mnt/obob/staff/aweise/data/crossfrog/data/meg/preproc/ica/' subj_id '/'];

if sss
    load(fullfile(inpath, ['file_0' num2str(block) '_sss']), 'ica_components', 'subj_id', 'block');
end

% inspect components...
cfg = [];
cfg.blocksize = 20; % 20 seconds of data segments to look at ( decrease to xxx in case this is more convenient)
cfg.viewmode = 'component';
cfg.layout = 'neuromag306mag.lay';

ft_databrowser(cfg, ica_components);

%% define components to be rejected and write them down manually to save them
disp('******************************************************************');
disp('*');
warning('your input required');
disp('*');
disp('******************************************************************');
prompt = 'Write down independent component you identified![y]: ';
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end

%%
rejected_components = []; % e.g. [1, 5];
disp('compontents to be rejected defined');

% save data...
if ~exist(outpath,'dir')
    mkdir(outpath);
end

if sss
     save(fullfile(outpath, ['file_0' num2str(block) '_sss_rej_comp']), 'rejected_components', 'subj_id', 'block');
end
disp('data saved');
rejected_components = [];

%% delete components to be on the save side and not save it for the next block / subj!
disp('******************************************************************');
disp('*');
warning('your input required');
disp('*');
disp('******************************************************************');
prompt = 'Delete components to be on the save side and NOT save it for the next block / subj![y]: ';
str = input(prompt,'s');
if isempty(str)
    str = 'Y';
end


