
% (C) A. Weise

%% clear workspace
clear all global;
close all;

%% init obob_ownft...
addpath('/mnt/obob/obob_ownft');

cfg = [];
cfg.package.hnc_condor = true;

obob_init_ft(cfg);

%% define paths
addpath('/mnt/obob/staff/aweise/data/crossfrog/scripts/');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
choose_sglTrl = 'no'; 

chooseTFmethod =  {'wavelet' } ; 

logtransform = 'yes';
data_type =  'meg';
subj_ids = functions.get_subject_ids(data_type);
choose_sensors = {  'all_sensors'  }; % take always 'all_sensors' as grads can be selected later in sensor space
epoch = {'sound'}; 
chooseCycle = 5; 
sss = true;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
logpath = '/mnt/obob/staff/aweise/data/crossfrog/data/meg/sensor/pow/log/';
 
if ~exist( logpath, 'dir')
    mkdir(logpath)
end

% ******************************************************

%% configure the job cluster
cfg = [];
cfg.mem = '8G';        % The amount of RAM required to run one job in the
%                         jobcluster. In "T", "G", "M" or "K".
cfg.adust_mem = true;   % default option;  job that gets held
%                         because it exceeded its requested memory will be
%                         resubmitted automatically after a couple of minutes
%                         with updated memory requirements
cfg.jobsdir = logpath;

condor_struct = obob_condor_create(cfg);

%% add jobs, i.e. call function: xyc
condor_struct = obob_condor_addjob_cell(condor_struct, 'cluster_functions.p02c_sensor_pow', subj_ids, sss, chooseTFmethod, chooseCycle, choose_sensors,  epoch, choose_sglTrl,logtransform);

%% submit job
obob_condor_submit(condor_struct);






