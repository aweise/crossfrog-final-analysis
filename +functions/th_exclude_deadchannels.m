function [ data ] = th_exclude_deadchannels( data )
%TH_EXCLUDE_DEADCHANNELS Summary of this function goes here
%   Detailed explanation goes here

tmp = cell2mat(data.trial);
xsig = abs(tmp) >= eps;

dead_threshold = 1000;

dead_chans = [];
for i = 1:size(xsig, 1)
  xsig_tmp = xsig(i, :);
  dsig = diff([1 xsig_tmp 1]);
  startI = find(dsig < 0);
  endI = find(dsig > 0);
  duration = endI - startI +1;
  toolong = duration(duration >= dead_threshold);
  if ~isempty(toolong)
    fprintf('Channel %d is dead.\n', i);
    dead_chans(end+1) = i;
  end %if
end %for

channels = data.label;
channels(dead_chans) = [];

cfg = [];
cfg.channel = channels;

data = ft_selectdata(cfg, data);

end

