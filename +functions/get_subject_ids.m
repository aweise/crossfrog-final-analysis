function [subj_ids] = get_subject_ids(data_type)


% (C) Anne Weise

% input: data_type:
%           (1) 'meg' 

% output: subj_ids


%% analyses for all subjects with good MEG data
% for MEG exclude  '19960619urgo' because of massive artifacts due to retainer

if strcmp(data_type, 'meg')
    subj_ids = { '19891203igfm', '19940810eehb', '19930423eehb', '19960409krfc', '19800908igdb', '19810918slbr', '19801021mrhl', '19930405ldwi', '19860211eisr', '19910612crke', '19950124gblu', '19961006sbhe', '19990109eeht','19981224smsk', '19930118imsh', '19960122urfo', '19840920gasa','19930105ieeg',...
        '19890709brgc', '19950913urgu', '19930113crfd', '19920712sbhi',...
        '19800616mrgu', '19831031krab', '19910610ludy', '19910711eeri', '19961112cahn',...
        '19750206hlbn', '19941011atsm', '19940808mrzc'  }; % subj ids for which standards were presented binaurally!
   
end



