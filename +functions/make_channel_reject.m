function [ channel_select ] = make_channel_reject( bad_channels )
%MAKE_CHANNEL_REJECT Summary of this function goes here
%   Detailed explanation goes here

channel_select = {'all'};

if isstr(bad_channels)
  bad_channels = {bad_channels};
end %if

for i = 1:length(bad_channels)
  channel_select{end+1} = sprintf('-%s', bad_channels{i});
end %for

end

