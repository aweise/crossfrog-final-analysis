function [cond_codes] = get_cond_codes(cond_type, epoch)

% Trigger       Deviant	  Right-Sound Right-Target	
% 2                           1           0           0	
% 3                           1           1           0	
% 4                           1           0           1	
% 5                           1           1           1	

if strcmp(epoch,'sound')    
    
    if strcmp(cond_type,'dev_left')
        cond_codes = [2,4];
    elseif strcmp(cond_type,'dev_right')
        cond_codes = [3,5];  
    elseif strcmp(cond_type,'allDev')
            cond_codes = [2,3,4,5];  
    end
    
end
    
    
    
