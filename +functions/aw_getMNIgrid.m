function [mni_grid] = aw_getMNIgrid(gridRes, inwardshift)

% (C) Anne Weise

% input: grid resolution in m
% output: mni_grid

aw_templatePath = '/mnt/obob/staff/aweise/templates';

% gridRes = 0.015; % --> 1.5 cm; for testing only
% inwardshift = -0.015; % --> -1.5 cm; for testing only

cfg             = [];
cfg.gridres     = gridres; % currently cfg.gridres = 0.015 m --> 1.5 cm; use smaller grid resolution (0.5) and constraint to grey matter! (see gitlab wiki how to); 
cfg.inwardshift = inwardshift;
% cfg.plot        = 'yes';

mni_grid   = functions.th_gm2_make_mnigrid(cfg);


nameGridRes = num2str(gridRes);

save([aw_templatePath '/mni_grid' nameGridRes '.mat' ], 'mni_grid');
