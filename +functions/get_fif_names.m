function fif_file = get_fif_names( subj_id, block, sss )


if ispc
    fif_base_folder = 'O:/mnt/sinuhe/data_raw/aw_crossfrog/subject_subject/';
else
    fif_base_folder = '/mnt/sinuhe/data_raw/aw_crossfrog/subject_subject/';
end %if

to_search = '%s_';

if strcmp(block, 'rest')
    to_search = [to_search 'rest'];
else
    to_search = [to_search sprintf('block%02d', block)];
end %if

if sss
    to_search = [to_search '_sss'];
end %if

to_search = [to_search '.fif'];

fif_file = functions.rdir(fullfile(fif_base_folder, '*', sprintf(to_search, subj_id)));
fif_file = fif_file.name;

end

