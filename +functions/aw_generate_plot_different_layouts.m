function aw_generate_plot_different_layouts

% (C) Annekathrin Weise, Feb 2017

doPlot = 'no' % 'yes' to make plots or 'no'
    
cfg = [];
cfg.layout = 'neuromag306cmb.lay'; % part of fieldtrip toolbox! make sure to have it in the path
layout.grad.all = ft_prepare_layout(cfg);

cfg.layout = 'neuromag306mag.lay';
layout.mag.all = ft_prepare_layout(cfg);

if strcmp(doPlot,'yes')
    figure(1)
    ft_plot_lay(layout.grad.all);
    title('neuromag306cmb');
    
    figure(2)
    ft_plot_lay(layout.mag.all);
    title('neuromag306mag');
end

channelSelections = {'left', 'right'};
sensTypes = {'grad', 'mag'};

figureNumber = 2;

%% generate new layout for specifi channel selection and for different sensors
for iSensType = 1:length(sensTypes)
    
    sensType = sensTypes{iSensType};
    
    for iChanSelect = 1:length(channelSelections)
        
        figureNumber = figureNumber + 1;
        
        chanSelect = channelSelections{iChanSelect};
        
        % get all channel positions
        allChannelPos = layout.(sensType).all.pos(:,1);
        
        % get the positions of selected channels only   
        if strcmp(chanSelect,'left')
            indLeftChannelPos = find(allChannelPos < 0.0006085 ); % indices of left channel positions manually determined
            %                                                       knowing that it must be smaller than zero and that it
            %                                                       must result in half of the channels stored in the original layout
        elseif strcmp(chanSelect,'right')
            indLeftChannelPos = find(allChannelPos > 0.0006085 );
        end
                
        layout.(sensType).(chanSelect) = layout.(sensType).all; % take over original format
        
        layout.(sensType).(chanSelect).pos = []; % clear what is not needed
        layout.(sensType).(chanSelect).pos(:,1) = layout.(sensType).all.pos(indLeftChannelPos,1); % fill relevant x coordinates
        layout.(sensType).(chanSelect).pos(:,2) = layout.(sensType).all.pos(indLeftChannelPos,2); % fill relevant y coordinates
        
        layout.(sensType).(chanSelect).width = []; % clear what is not needed
        layout.(sensType).(chanSelect).width = layout.(sensType).all.width(indLeftChannelPos,1);
        
        layout.(sensType).(chanSelect).height = []; % clear what is not needed
        layout.(sensType).(chanSelect).height = layout.(sensType).all.height(indLeftChannelPos,1);
        
        layout.(sensType).(chanSelect).label = []; % clear what is not needed
        layout.(sensType).(chanSelect).label = layout.(sensType).all.label(indLeftChannelPos,1);
        
        if strcmp(doPlot,'yes')
            figure(figureNumber)
            ft_plot_lay(layout.(sensType).(chanSelect));
            
            if strcmp(sensType,'grad')
                title(['neuromag306cmb ' chanSelect]);
            elseif strcmp(sensType,'mag')
                title(['neuromag306mag ' chanSelect]);
            end
        end
    end
end
save('aw_neuromag306layout.mat', 'layout');

%% Testing
if strcmp(doPlot,'yes')
    load('aw_neuromag306layout');
    test = layout.grad.left;
    test.outline{1,4}=[]; % delete left ear
    test.outline{1,3}=[]; % delete right ear
    % test.outline{1,2}(1,:) = 0; % delete right nose part
    test.outline{1,2}(3,:) = 0; % delete left nose part
    % test.outline{1,1}(52:end,:) = 0; % work on that to delete half of the circle (left side)
    % test.outline{1,1}(52:end,:) = 0; % work on that to delete half of the circle (right side)
    
    figure(4)
    ft_plot_lay(test)
end
