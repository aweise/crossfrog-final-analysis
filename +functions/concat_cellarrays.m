function [ out ] = concat_cellarrays(varargin)

for idx_arg = 1:length(varargin)
  varargin{idx_arg} = varargin{idx_arg}(:);
end %for

out = vertcat(varargin{:});

end

